# XJCO 1921 Programming Project - Coursework 1 

this is a software program in C for the management of a library.This project allows new users to register in the system and allows users to search for books by title, author or year of publication, including borrowing and returning books. This project also provides a convenient way for librarians to add and remove books from the library, and to check the status of books at any time.
## URL
https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/edit/main/-/README.md

## Submission History Screenshot
![Image text](https://gitlab.com/DengLeixin/xjco-1921-programming-project-coursework-1/-/blob/main/Submit%20photos/1.png)
![Image text](https://gitlab.com/DengLeixin/xjco-1921-programming-project-coursework-1/-/blob/main/Submit%20photos/2.png)
![Image text](https://gitlab.com/DengLeixin/xjco-1921-programming-project-coursework-1/-/blob/main/Submit%20photos/3.png)

## Name
DLX Library system

## Description
This project allows new users to register in the system and allows users to search for books by title, author or year of publication, including borrowing and returning books. This project also provides a convenient way for librarians to add and remove books from the library, and to check the status of books at any time.

## Visuals
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/Main.png)
## Installation
The project is written in C and the code is compiled using Makefile.
## Usage
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/1.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/2.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/3.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/4.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/5.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/6.png)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/7.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/8.jpg)
![Image text](https://gitlab.com/-/ide/project/DengLeixin/xjco-1921-programming-project-coursework-1/tree/main/-/Testphotos/9.jpg)
## Support
If you have good suggestions for changes or new features to add, you can always contact me by email at sc20l2d@leeds.ac.uk
## Roadmap
Future versions may implement time limits and provide borrower contact information to facilitate librarians to contact and deal with borrowers who do not return books in a timely manner. If you have better ideas, please feel free to contact my team by email to let us know your ideas!

## Contributing
My project is open to contributions, and you can make changes to my program as long as you can optimize it and can enhance its usability and usefulness
## Authors and acknowledgment
The author of this project is Deng Leixin, an undergraduate student from Leeds College, Southwest Jiaotong University and the University of Leeds, China. Special thanks to all the teachers and teaching assistants at XJCO1921 for their patience in answering the questions I encountered during the writing process.

## Project status
The project has been successfully implemented and can run the following functions, and is in the process of continuous refinement and improvement
